package itree.demo.controller;

import com.sise.itree.common.BaseController;
import com.sise.itree.common.annotation.ControllerMapping;
import com.sise.itree.core.handle.response.BaseResponse;
import com.sise.itree.model.ControllerRequest;

/**
 * @author idea
 * @data 2019/4/30
 */
@ControllerMapping(url = "/myController")
public class MyController implements BaseController {

    @Override
    public BaseResponse doGet(ControllerRequest controllerRequest) {
        String username= (String) controllerRequest.getParameter("username");
        System.out.println(username);
        return new BaseResponse(1,username);
    }

    @Override
    public BaseResponse doPost(ControllerRequest controllerRequest) {
        return null;
    }
}
